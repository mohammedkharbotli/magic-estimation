<?php
declare(strict_types = 1);

namespace App\Util;

use App\Model\Field;
use Webmozart\Assert\Assert;

class FieldMapper implements ResponseMapperInterface
{

    private array $data;

    public function __construct(array $data)
    {
        Assert::allIsArray($data);
        $this->data = $data;
    }

    public function map(): array
    {
        $fields = array_map(function (array $field) {
            Assert::keyExists($field, 'id');
            Assert::keyExists($field, 'name');

            return new Field($field['id'], $field['name']);
        }, $this->data);
        if ($fields) {
            Assert::allIsInstanceOf($fields, Field::class);
        }

        return $fields;
    }

}
