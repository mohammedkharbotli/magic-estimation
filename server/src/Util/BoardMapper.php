<?php
declare(strict_types = 1);

namespace App\Util;

use App\Model\Board;
use Webmozart\Assert\Assert;

class BoardMapper implements ResponseMapperInterface
{

    private array $boards;

    public function __construct(array $data)
    {
        Assert::keyExists($data, 'values');
        Assert::isArray($data['values']);

        $this->boards = $data['values'];
    }

    public function map(): array
    {
        $boards = array_map(function (array $board) {
            Assert::keyExists($board, 'id');
            Assert::keyExists($board, 'name');

            return new Board($board['id'], $board['name']);
        }, $this->boards);
        if ($boards) {
            Assert::allIsInstanceOf($boards, Board::class);
        }

        return $boards;
    }

}
