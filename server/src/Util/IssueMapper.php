<?php
declare(strict_types = 1);

namespace App\Util;

use App\Model\Assignee;
use App\Model\Issue;
use App\Model\IssueType;
use Webmozart\Assert\Assert;

class IssueMapper implements ResponseMapperInterface
{

    private array $issues;

    public function __construct(array $data)
    {
        Assert::keyExists($data, 'issues');
        Assert::isArray($data['issues']);

        $this->issues = $data['issues'];
    }

    public function map(): array
    {
        $issues = array_map(function (array $issue) {
            Assert::keyExists($issue, 'id');
            Assert::keyExists($issue, 'key');
            Assert::keyExists($issue, 'fields');
            Assert::isArray($issue['fields']);
            $fields = $issue['fields'];
            Assert::keyExists($fields, 'issuetype');
            Assert::isArray($fields['issuetype']);
            $type = $fields['issuetype'];
            Assert::keyExists($type, 'id');
            Assert::keyExists($type, 'iconUrl');
            Assert::keyExists($type, 'name');
            Assert::keyExists($fields, 'created');
            Assert::keyExists($fields, 'updated');
            Assert::keyExists($fields, 'assignee');
            Assert::keyExists($fields, 'status');
            Assert::isArray($fields['status']);
            Assert::keyExists($fields['status'], 'name');
            Assert::keyExists($fields, 'description');
            Assert::keyExists($fields, 'summary');
            Assert::keyExists($fields, 'sprint');
            Assert::isArray($fields['sprint']);
            Assert::keyExists($fields['sprint'], 'id');
            Assert::numeric($fields['sprint']['id']);

            $assignee = $this->extractAssignee($fields);

            return new Issue(
                $issue['id'],
                $issue['key'],
                new IssueType($fields['issuetype']['id'], $fields['issuetype']['name'], $fields['issuetype']['iconUrl']),
                $fields['created'],
                $assignee,
                $fields['updated'],
                $fields['status']['name'],
                $fields['description'],
                $fields['summary'],
                (int) $fields['sprint']['id']
            );
        }, $this->issues);

        Assert::allIsInstanceOf($issues, Issue::class);

        return $issues;
    }

    private function extractAssignee(array $fields): ?Assignee
    {
        if (
            $fields['assignee'] &&
            is_array($fields['assignee']) &&
            key_exists('avatarUrls', $fields['assignee']) &&
            key_exists('displayName', $fields['assignee'])
        ) {
            return new Assignee($fields['assignee']['avatarUrls'], $fields['assignee']['displayName']);
        }

        return null;
    }

}
