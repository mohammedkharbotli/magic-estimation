<?php
declare(strict_types = 1);

namespace App\Util;

use App\Model\Sprint;
use Webmozart\Assert\Assert;

class SprintMapper implements ResponseMapperInterface
{

    private array $sprints;

    public function __construct(array $data)
    {
        Assert::isArray($data);

        $this->sprints = $data;
    }

    /**
     * @return Sprint[]
     */
    public function map(): array
    {
        Assert::keyExists($this->sprints, 'values');
        Assert::isArray($this->sprints['values']);

        return array_map(function (array $sprint) {

            return $this->buildSprint($sprint);
        }, $this->sprints['values']);
    }

    public function mapOne(): sprint
    {
        return $this->buildSprint($this->sprints);
    }

    private function buildSprint(array $sprint): Sprint
    {
        Assert::keyExists($sprint, 'id');
        Assert::keyExists($sprint, 'name');
        Assert::keyExists($sprint, 'state');
        Assert::keyExists($sprint, 'originBoardId');
        Assert::numeric($sprint['id']);

        return new Sprint((int) $sprint['id'], $sprint['state'], $sprint['name'], $sprint['goal'] ?? '', (string) $sprint['originBoardId']);
    }

}
