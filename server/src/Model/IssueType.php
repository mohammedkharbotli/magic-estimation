<?php
declare(strict_types = 1);

namespace App\Model;

class IssueType
{

    public string    $id;

    public string    $name;

    public string    $iconUrl;

    public function __construct(string $id, string $name, string $iconUrl)
    {
        $this->id      = $id;
        $this->name    = $name;
        $this->iconUrl = $iconUrl;
    }

}
