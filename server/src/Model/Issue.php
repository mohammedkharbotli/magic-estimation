<?php

namespace App\Model;

class Issue
{

    public string        $id;

    public string        $key;

    public IssueType     $type;

    public string        $created;

    public ?Assignee     $assignee;

    public string        $updated;

    public string        $status;

    public ?string       $description;

    public string        $title;

    public int           $sprintId;

    public function __construct(
        string $id,
        string $key,
        IssueType $type,
        string $created,
        ?Assignee $assignee,
        string $updated,
        string $status,
        ?string $description,
        string $title,
        int $sprintId
    ) {
        $this->id          = $id;
        $this->key         = $key;
        $this->type        = $type;
        $this->created     = $created;
        $this->assignee    = $assignee;
        $this->updated     = $updated;
        $this->status      = $status;
        $this->description = $description;
        $this->title       = $title;
        $this->sprintId    = $sprintId;
    }

}
