<?php
declare(strict_types = 1);

namespace App\Model;

class Assignee
{

    public array  $avatarUrls;

    public string $displayName;

    public function __construct(array $avatarUrls, string $displayName)
    {
        $this->avatarUrls  = $avatarUrls;
        $this->displayName = $displayName;
    }

}
