<?php

namespace App\Model;

class Sprint
{

    public int     $id;

    public string  $state;

    public string  $name;

    public string  $goal;

    public string $board;

    public function __construct(int $id, string $state, string $name, string $goal, string $board)
    {
        $this->id    = $id;
        $this->state = $state;
        $this->name  = $name;
        $this->goal  = $goal;
        $this->board = $board;
    }

}
