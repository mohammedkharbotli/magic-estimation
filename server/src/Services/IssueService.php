<?php
declare(strict_types = 1);

namespace App\Services;

use App\Model\Field;
use App\Model\Issue;
use App\Util\FieldMapper;
use App\Util\IssueMapper;
use GuzzleHttp\Client;
use Webmozart\Assert\Assert;

class IssueService
{

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $sprintId
     * @return Issue[]
     */
    public function getIssues(string $sprintId): array
    {
        $url          = sprintf('rest/agile/latest/sprint/%s/issue', $sprintId);
        $jiraResponse = $this->client->get($url)->getBody()->getContents();
        $data         = json_decode($jiraResponse, true);
        $mapper       = new IssueMapper($data);

        return $mapper->map();
    }

    public function getStoryPointFieldId(): Field
    {
        $jiraResponse = $this->client->get('rest/api/latest/field')->getBody()->getContents();
        $data         = json_decode($jiraResponse, true);
        $mapper       = new FieldMapper($data);
        $fields       = $mapper->map();
        $storyPoint   = array_filter($fields, fn(Field $field) => $field->name === 'Story Points');
        Assert::count($storyPoint, 1);

        return current($storyPoint);
    }

    public function extractAndUpdateIssues(array $data, string $fieldId): void
    {
        Assert::keyExists($data, 'groups');
        foreach ($data['groups'] as $group) {
            Assert::keyExists($group, 'model');
            Assert::keyExists($group, 'value');
            Assert::isArray($group['model']);
            $issues     = $group['model'];
            $fieldValue = $group['value'];
            Assert::numeric($fieldValue);
            foreach ($issues as $issue) {
                Assert::isArray($issue);
                Assert::keyExists($issue, 'id');
                $this->updateIssue($issue['id'], $fieldId, (float) $fieldValue);
            }
        }
    }

    private function updateIssue(string $issueId, string $fieldId, float $fieldValue): void
    {
        $url = sprintf('rest/api/latest/issue/%s', $issueId);
        $this->client->put($url, [
            'json' => [
                "fields" => [
                    $fieldId => $fieldValue,
                ],
            ],
        ]);
    }

}
