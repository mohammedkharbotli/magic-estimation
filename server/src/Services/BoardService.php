<?php
declare(strict_types = 1);

namespace App\Services;

use App\Model\Board;
use App\Util\BoardMapper;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

class BoardService
{

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return Board[]
     */
    public function getBoards(): array
    {
        $jiraResponse = $this->client->get('rest/agile/1.0/board')->getBody()->getContents();
        $data         = json_decode($jiraResponse, true);
        $mapper       = new BoardMapper($data);

        return $mapper->map();
    }

    public function checkBoardExists(string $boardId): bool
    {
        $url      = sprintf('rest/agile/latest/board/%s', $boardId);
        $response = $this->client->get($url);

        return ($response->getStatusCode() === Response::HTTP_OK);
    }

}
