<?php
declare(strict_types = 1);

namespace App\Services;

use App\Model\Sprint;
use App\Util\SprintMapper;
use GuzzleHttp\Client;

class SprintService
{

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getSprints(string $boardId): array
    {
        $url          = sprintf('rest/agile/latest/board/%s/sprint', $boardId);
        $jiraResponse = $this->client->get($url)->getBody()->getContents();
        $data         = json_decode($jiraResponse, true);
        $mapper       = new SprintMapper($data);

        return $mapper->map();
    }

    public function getSprintById(string $sprintId): Sprint
    {
        $url          = sprintf('rest/agile/latest/sprint/%s', $sprintId);
        $jiraResponse = $this->client->get($url)->getBody()->getContents();
        $data         = json_decode($jiraResponse, true);
        $mapper       = new SprintMapper($data);

        return $mapper->mapOne();
    }

    public function updateSprintGoal(string $sprintId, string $goal): void
    {
        $url = sprintf('rest/agile/latest/sprint/%s', $sprintId);
        $this->client->post($url, [
            'json' => [
                "goal" => $goal,
            ],
        ]);
    }

}
