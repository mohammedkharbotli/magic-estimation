<?php
declare(strict_types = 1);

namespace App\Controller\Board;

use App\Services\BoardService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/")
 */
class GetController
{

    private BoardService $boardService;

    public function __construct(BoardService $boardService)
    {
        $this->boardService = $boardService;
    }

    /**
     * @Route("boards/", name="boards.get.all", methods={"GET"})
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        $board = $this->boardService->getBoards();

        return new JsonResponse($board);
    }

}
