<?php
declare(strict_types = 1);

namespace App\Controller\Issues;

use App\Services\IssueService;
use App\Services\SprintService;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/")
 */
class UpdateIssueController
{

    private SprintService $sprintService;

    private IssueService  $issueService;

    public function __construct(SprintService $sprintService, IssueService $issueService)
    {
        $this->sprintService = $sprintService;
        $this->issueService  = $issueService;
    }

    /**
     * @Route("issue/{sprintId}/update/", methods={"POST"}, name="issue.update")
     * @param Request $request
     * @param string  $sprintId
     * @return JsonResponse
     */
    public function __invoke(Request $request, string $sprintId): JsonResponse
    {
        $content = json_decode($request->getContent(), true);
        if (!key_exists('goal', $content) || !key_exists('groups', $content)) {
            return new JsonResponse('something went wrong', Response::HTTP_BAD_REQUEST);
        }

        try {
            $field = $this->issueService->getStoryPointFieldId();
        } catch (Exception $e) {
            return new JsonResponse('field is not activated, please modify your jira configuration', Response::HTTP_BAD_REQUEST);
        }

        try {
            $this->issueService->extractAndUpdateIssues($content, $field->id);
            $this->sprintService->updateSprintGoal($sprintId, $content['goal']);
        } catch (Exception $e) {
            return new JsonResponse('something went wrong', Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(null, Response::HTTP_OK);
    }

}
