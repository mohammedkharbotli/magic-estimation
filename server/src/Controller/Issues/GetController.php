<?php
declare(strict_types = 1);

namespace App\Controller\Issues;

use App\Model\Sprint;
use App\Services\IssueService;
use App\Services\SprintService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/")
 */
class GetController
{

    private SprintService $sprintService;

    private IssueService  $issueService;

    public function __construct(SprintService $sprintService, IssueService $issueService)
    {
        $this->sprintService = $sprintService;
        $this->issueService  = $issueService;
    }

    /**
     * @Route("issues/{sprintId}/", name="issues.get.all", methods={"GET"})
     * @param string $sprintId
     * @return JsonResponse
     */
    public function __invoke(string $sprintId): JsonResponse
    {
        if (!is_numeric($sprintId)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $sprint = $this->sprintService->getSprintById($sprintId);
        } catch (\Exception $e) {
            $status = ($e->getCode() === Response::HTTP_NOT_FOUND) ? Response::HTTP_NOT_FOUND : Response::HTTP_INTERNAL_SERVER_ERROR;

            return new JsonResponse(null, $status);
        }

        if (!$sprint instanceof Sprint || !$sprint->id === (int) $sprintId) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        $issues = $this->issueService->getIssues($sprintId);

        return new JsonResponse($issues);
    }

}
