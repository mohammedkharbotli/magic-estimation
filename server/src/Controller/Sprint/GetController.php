<?php
declare(strict_types = 1);

namespace App\Controller\Sprint;

use App\Model\Sprint;
use App\Services\BoardService;
use App\Services\SprintService;
use App\Util\SprintMapper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/")
 */
class GetController
{

    private BoardService  $boardService;

    private SprintService $sprintService;

    public function __construct(BoardService $boardService, SprintService $sprintService)
    {
        $this->boardService  = $boardService;
        $this->sprintService = $sprintService;
    }

    /**
     * @Route("board/{board}/sprint", name="board.get.sprints", methods={"GET"})
     * @param string $board
     * @return JsonResponse
     */
    public function __invoke(string $board): JsonResponse
    {
        if (!is_numeric($board)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $this->boardService->checkBoardExists($board);
        } catch (\Exception $e) {
            $status = ($e->getCode() === Response::HTTP_NOT_FOUND) ? Response::HTTP_NOT_FOUND : Response::HTTP_INTERNAL_SERVER_ERROR;

            return new JsonResponse(null, $status);
        }

        try {
            $sprints = $this->sprintService->getSprints($board);

            return new JsonResponse($sprints);
        } catch (\Exception $e) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }
    }

}
