<?php
declare(strict_types = 1);

namespace App\Controller\Sprint;

use App\Model\Sprint;
use App\Services\SprintService;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/")
 */
class CheckController
{

    private SprintService $sprintService;

    public function __construct(SprintService $sprintService)
    {
        $this->sprintService = $sprintService;
    }

    /**
     * @Route("sprint/check/{boardId}/{sprintId}", name="sprint.check", methods={"GET"})
     * @param string $boardId
     * @param string $sprintId
     * @return JsonResponse
     */
    public function __invoke(string $boardId, string $sprintId): JsonResponse
    {
        if (!is_numeric($sprintId) || !is_numeric($boardId)) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        try {
            $sprint = $this->sprintService->getSprintById($sprintId);
        } catch (Exception $e) {
            $status = ($e->getCode() === Response::HTTP_NOT_FOUND) ? Response::HTTP_NOT_FOUND : Response::HTTP_INTERNAL_SERVER_ERROR;

            return new JsonResponse(null, $status);
        }

        if (!$sprint instanceof Sprint || !$sprint->id === (int) $sprintId || $sprint->board !== $boardId) {
            return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['goal' => $sprint->goal], Response::HTTP_OK);
    }

}
