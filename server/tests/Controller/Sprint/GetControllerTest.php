<?php
declare(strict_types = 1);

namespace App\Controller\Sprint;

use Generator;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class GetControllerTest extends WebTestCase
{

    private KernelBrowser $client;

    /**
     * * @dataProvider data_provider
     * @param string $method
     * @param int    $status
     */
    public function testWrongRequestMethod(string $method, int $status): void
    {
        $this->client->request($method, '/api/board/1/sprint');
        self::assertEquals($status, $this->client->getResponse()->getStatusCode());
    }

    public function testIfIdIsNotNumeric(): void
    {
        $this->client->request('GET', '/api/board/test/sprint');
        self::assertEquals(Response::HTTP_BAD_REQUEST, $this->client->getResponse()->getStatusCode());
    }

    public function testBoardDoesNotExist(): void
    {
        $this->client->request('GET', '/api/board/10010101001/sprint');
        self::assertEquals(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    public function data_provider(): Generator
    {
        yield ['POST', Response::HTTP_METHOD_NOT_ALLOWED,];
        yield ['PUT', Response::HTTP_METHOD_NOT_ALLOWED,];
        yield ['DELETE', Response::HTTP_METHOD_NOT_ALLOWED,];
    }

    protected function setUp(): void
    {
        $this->client = self::createClient();
    }

}
