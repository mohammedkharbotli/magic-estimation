<?php
declare(strict_types = 1);

namespace Tests\Controller\Board;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class GetControllerTest extends WebTestCase
{

    public function testShowPost(): void
    {
        $client = self::createClient();
        $client->request('GET', '/api/boards/');
        self::assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

}
