<?php
declare(strict_types = 1);

namespace Tests\Controller\Issues;

use Generator;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UpdateIssueControllerTest extends WebTestCase
{

    private KernelBrowser $client;

    /**
     * * @dataProvider data_provider
     * @param string $method
     * @param int    $status
     */
    public function testWrongRequestMethod(string $method, int $status): void
    {
        $this->client->request($method, '/api/issue/1/update/');
        self::assertEquals($status, $this->client->getResponse()->getStatusCode());
    }

    public function data_provider(): Generator
    {
        yield ['GET', Response::HTTP_METHOD_NOT_ALLOWED];
        yield ['PUT', Response::HTTP_METHOD_NOT_ALLOWED];
        yield ['DELETE', Response::HTTP_METHOD_NOT_ALLOWED];
    }

    protected function setUp(): void
    {
        $this->client = self::createClient();
    }

}
