<?php
declare(strict_types = 1);

namespace Tests\Util;

use App\Model\Board;
use App\Util\BoardMapper;
use Generator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TypeError;

class BoardMapperTest extends TestCase
{

    /**
     * * @dataProvider constructor_data_provider
     * @param mixed  $data
     * @param string $expectedError
     */
    public function testPassWrongValueWoConstructor($data, string $expectedError)
    {
        self::expectException($expectedError);
        new BoardMapper($data);
    }

    /**
     * * @dataProvider map_data_provider
     * @param mixed  $data
     * @param string $expectedError
     */
    public function testMapWrongData(array $data, string $expectedError)
    {
        self::expectException($expectedError);
        $mapper = new BoardMapper($data);

        $mapper->map();
    }

    public function testWithCorrectData()
    {
        $data   = [
            "maxResults" => 50,
            "startAt"    => 0,
            "total"      => 1,
            "isLast"     => true,
            "values"     => [
                [
                    "id"   => 1,
                    "self" => "http://jira.magic-estimation.dev.local/rest/agile/1.0/board/1",
                    "name" => "AZH board",
                    "type" => "scrum",
                ],
                [
                    "id"   => 2,
                    "self" => "http://jira.magic-estimation.dev.local/rest/agile/1.0/board/2",
                    "name" => "MEP board",
                    "type" => "scrum",
                ],
            ],
        ];
        $mapper = new BoardMapper($data);
        $boards = $mapper->map();
        self::assertIsArray($boards);
        self::assertCount(2, $boards);
        self::assertContainsOnlyInstancesOf(Board::class, $boards);
    }

    public function constructor_data_provider(): Generator
    {
        yield [111, TypeError::class,];
        yield ['string', TypeError::class,];
        yield [(object) 'test', TypeError::class,];
        yield [3.3, TypeError::class];
        yield [[], InvalidArgumentException::class];
        yield [[], InvalidArgumentException::class];
        yield [['values' => 'values'], InvalidArgumentException::class];
    }

    public function map_data_provider(): Generator
    {
        yield [['values' => ['id' => '']], TypeError::class];
        yield [['values' => [['name' => '']]], InvalidArgumentException::class];
        yield [['values' => [['id' => '']]], InvalidArgumentException::class];
    }

}
