<?php
declare(strict_types = 1);

namespace Tests\Util;

use App\Model\Field;
use App\Util\FieldMapper;
use Generator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TypeError;

class FieldMapperTest extends TestCase
{

    /**
     * * @dataProvider constructor_data_provider
     * @param mixed  $data
     * @param string $expectedError
     */
    public function testPassWrongValueWoConstructor($data, string $expectedError)
    {
        self::expectException($expectedError);
        new FieldMapper($data);
    }

    /**
     * * @dataProvider map_data_provider
     * @param mixed  $data
     * @param string $expectedError
     */
    public function testMapWrongData(array $data, string $expectedError)
    {
        self::expectException($expectedError);
        $mapper = new FieldMapper($data);

        $mapper->map();
    }

    public function testWithCorrectData()
    {
        $data   = [
            [
                'id'   => '10000',
                'name' => 'PROJ-1',
            ],
            [
                'id'   => '20000',
                'name' => 'PROJ-2',
            ],
        ];
        $mapper = new FieldMapper($data);
        $fields = $mapper->map();
        self::assertIsArray($fields);
        self::assertCount(2, $fields);
        self::assertContainsOnlyInstancesOf(Field::class, $fields);
    }

    public function constructor_data_provider(): Generator
    {
        yield [111, TypeError::class,];
        yield ['string', TypeError::class,];
        yield [(object) 'test', TypeError::class,];
        yield [['test' => 'test'], InvalidArgumentException::class];
        yield [3.3, TypeError::class];
    }

    public function map_data_provider(): Generator
    {
        yield [[['name' => '']], InvalidArgumentException::class];
        yield [[['id' => '']], InvalidArgumentException::class];
    }

}
