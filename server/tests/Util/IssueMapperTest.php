<?php
declare(strict_types = 1);

namespace Tests\Util;

use App\Model\Issue;
use App\Util\IssueMapper;
use Generator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TypeError;

class IssueMapperTest extends TestCase
{

    /**
     * * @dataProvider constructor_data_provider
     * @param mixed  $data
     * @param string $expectedError
     */
    public function testPassWrongValueWoConstructor($data, string $expectedError)
    {
        self::expectException($expectedError);
        new IssueMapper($data);
    }

    /**
     * * @dataProvider map_data_provider
     * @param mixed  $data
     * @param string $expectedError
     */
    public function testMapWrongData(array $data, string $expectedError)
    {
        self::expectException($expectedError);
        $mapper = new IssueMapper($data);

        $mapper->map();
    }

    public function testWithCorrectData()
    {
        $data   = [
            'expand'     => 'schema,names',
            'startAt'    => 0,
            'maxResults' => 50,
            'total'      => 7,
            'issues'     =>
                [
                    [
                        'expand' => 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
                        'id'     => '10000',
                        'self'   => 'http://jira.magic-estimation.dev.local/rest/agile/1.0/issue/10000',
                        'key'    => 'PROJ-1',
                        'fields' =>
                            [
                                'issuetype'                     =>
                                    [
                                        'self'        => 'http://jira.magic-estimation.dev.local/rest/api/2/issuetype/10004',
                                        'id'          => '10004',
                                        'description' => 'A problem which impairs or prevents the functions of the product.',
                                        'iconUrl'     => 'http://jira.magic-estimation.dev.local/secure/viewavatar?size=xsmall&avatarId=10303&avatarType=issuetype',
                                        'name'        => 'Bug',
                                        'subtask'     => false,
                                        'avatarId'    => 10303,
                                    ],
                                'timespent'                     => NULL,
                                'timeoriginalestimate'          => NULL,
                                'sprint'                        =>
                                    [
                                        'id'            => 1,
                                        'self'          => 'http://jira.magic-estimation.dev.local/rest/agile/1.0/sprint/1',
                                        'state'         => 'future',
                                        'name'          => 'PROJ Sprint 1',
                                        'originBoardId' => 1,
                                        'goal'          => null,
                                    ],
                                'description'                   => 'Creating',
                                'project'                       =>
                                    [
                                        'self'           => 'http://jira.magic-estimation.dev.local/rest/api/2/project/10000',
                                        'id'             => '10000',
                                        'key'            => 'PROJ',
                                        'name'           => 'Project',
                                        'projectTypeKey' => 'software',
                                        'avatarUrls'     =>
                                            [
                                                '48x48' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?avatarId=10324',
                                                '24x24' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?size=small&avatarId=10324',
                                                '16x16' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?size=xsmall&avatarId=10324',
                                                '32x32' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?size=medium&avatarId=10324',
                                            ],
                                    ],
                                'aggregatetimespent'            => NULL,
                                'resolution'                    => NULL,
                                'customfield_10106'             => 0.5,
                                'aggregatetimeestimate'         => NULL,
                                'flagged'                       => false,
                                'resolutiondate'                => NULL,
                                'workratio'                     => -1,
                                'summary'                       => 'REST ye merry gentlemen.',
                                'lastViewed'                    => '2020-03-26T09:22:25.173+0000',
                                'watches'                       =>
                                    [
                                        'self'       => 'http://jira.magic-estimation.dev.local/rest/api/2/issue/PROJ-1/watchers',
                                        'watchCount' => 1,
                                        'isWatching' => true,
                                    ],
                                'creator'                       =>
                                    [
                                        'self'         => 'http://jira.magic-estimation.dev.local/rest/api/2/user?username=mohammed.kharbotli',
                                        'name'         => 'mohammed.kharbotli',
                                        'key'          => 'JIRAUSER10000',
                                        'emailAddress' => 'mohammed.kharbotli@gmail.com',
                                        'avatarUrls'   =>
                                            [
                                                '48x48' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=48',
                                                '24x24' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=24',
                                                '16x16' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=16',
                                                '32x32' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=32',
                                            ],
                                        'displayName'  => 'mohammed kharbotli',
                                        'active'       => true,
                                        'timeZone'     => 'GMT',
                                    ],
                                'created'                       => '2020-03-23T15:03:18.000+0000',
                                'reporter'                      =>
                                    [
                                        'self'         => 'http://jira.magic-estimation.dev.local/rest/api/2/user?username=mohammed.kharbotli',
                                        'name'         => 'mohammed.kharbotli',
                                        'key'          => 'JIRAUSER10000',
                                        'emailAddress' => 'mohammed.kharbotli@gmail.com',
                                        'avatarUrls'   =>
                                            [
                                                '48x48' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=48',
                                                '24x24' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=24',
                                                '16x16' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=16',
                                                '32x32' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=32',
                                            ],
                                        'displayName'  => 'mohammed kharbotli',
                                        'active'       => true,
                                        'timeZone'     => 'GMT',
                                    ],
                                'aggregateprogress'             =>
                                    [
                                        'progress' => 0,
                                        'total'    => 0,
                                    ],
                                'priority'                      =>
                                    [
                                        'self'    => 'http://jira.magic-estimation.dev.local/rest/api/2/priority/3',
                                        'iconUrl' => 'http://jira.magic-estimation.dev.local/images/icons/priorities/medium.svg',
                                        'name'    => 'Medium',
                                        'id'      => '3',
                                    ],
                                'customfield_10100'             => '0|hzzzzz:',
                                'customfield_10101'             => NULL,
                                'environment'                   => NULL,
                                'timeestimate'                  => NULL,
                                'aggregatetimeoriginalestimate' => NULL,
                                'duedate'                       => NULL,
                                'progress'                      =>
                                    [
                                        'progress' => 0,
                                        'total'    => 0,
                                    ],
                                'votes'                         =>
                                    [
                                        'self'     => 'http://jira.magic-estimation.dev.local/rest/api/2/issue/PROJ-1/votes',
                                        'votes'    => 0,
                                        'hasVoted' => false,
                                    ],
                                'worklog'                       =>
                                    [
                                        'startAt'    => 0,
                                        'maxResults' => 20,
                                        'total'      => 0,
                                    ],
                                'assignee'                      => NULL,
                                'updated'                       => '2020-03-26T09:23:55.000+0000',
                                'status'                        =>
                                    [
                                        'self'           => 'http://jira.magic-estimation.dev.local/rest/api/2/status/10000',
                                        'description'    => '',
                                        'iconUrl'        => 'http://jira.magic-estimation.dev.local/',
                                        'name'           => 'To Do',
                                        'id'             => '10000',
                                        'statusCategory' =>
                                            [
                                                'self'      => 'http://jira.magic-estimation.dev.local/rest/api/2/statuscategory/2',
                                                'id'        => 2,
                                                'key'       => 'new',
                                                'colorName' => 'blue-gray',
                                                'name'      => 'To Do',
                                            ],
                                    ],
                            ],
                    ],
                    [
                        'expand' => 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
                        'id'     => '10001',
                        'self'   => 'http://jira.magic-estimation.dev.local/rest/agile/1.0/issue/10001',
                        'key'    => 'PROJ-2',
                        'fields' =>
                            [
                                'issuetype'                     =>
                                    [
                                        'self'        => 'http://jira.magic-estimation.dev.local/rest/api/2/issuetype/10001',
                                        'id'          => '10001',
                                        'description' => 'Created by Jira Software - do not edit or delete. Issue type for a user story.',
                                        'iconUrl'     => 'http://jira.magic-estimation.dev.local/images/icons/issuetypes/story.svg',
                                        'name'        => 'Story',
                                        'subtask'     => false,
                                    ],
                                'timespent'                     => NULL,
                                'timeoriginalestimate'          => NULL,
                                'sprint'                        =>
                                    [
                                        'id'            => 1,
                                        'self'          => 'http://jira.magic-estimation.dev.local/rest/agile/1.0/sprint/1',
                                        'state'         => 'future',
                                        'name'          => 'PROJ Sprint 1',
                                        'originBoardId' => 1,
                                        'goal'          => null,
                                    ],
                                'description'                   => NULL,
                                'project'                       =>
                                    [
                                        'self'           => 'http://jira.magic-estimation.dev.local/rest/api/2/project/10000',
                                        'id'             => '10000',
                                        'key'            => 'PROJ',
                                        'name'           => 'Project',
                                        'projectTypeKey' => 'software',
                                        'avatarUrls'     =>
                                            [
                                                '48x48' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?avatarId=10324',
                                                '24x24' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?size=small&avatarId=10324',
                                                '16x16' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?size=xsmall&avatarId=10324',
                                                '32x32' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?size=medium&avatarId=10324',
                                            ],
                                    ],
                                'aggregatetimespent'            => NULL,
                                'resolution'                    => NULL,
                                'customfield_10106'             => 0.5,
                                'aggregatetimeestimate'         => NULL,
                                'flagged'                       => false,
                                'resolutiondate'                => NULL,
                                'workratio'                     => -1,
                                'summary'                       => 'some other ticket',
                                'lastViewed'                    => '2020-03-26T09:22:20.799+0000',
                                'watches'                       =>
                                    [
                                        'self'       => 'http://jira.magic-estimation.dev.local/rest/api/2/issue/PROJ-2/watchers',
                                        'watchCount' => 1,
                                        'isWatching' => true,
                                    ],
                                'creator'                       =>
                                    [
                                        'self'         => 'http://jira.magic-estimation.dev.local/rest/api/2/user?username=mohammed.kharbotli',
                                        'name'         => 'mohammed.kharbotli',
                                        'key'          => 'JIRAUSER10000',
                                        'emailAddress' => 'mohammed.kharbotli@gmail.com',
                                        'avatarUrls'   =>
                                            [
                                                '48x48' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=48',
                                                '24x24' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=24',
                                                '16x16' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=16',
                                                '32x32' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=32',
                                            ],
                                        'displayName'  => 'mohammed kharbotli',
                                        'active'       => true,
                                        'timeZone'     => 'GMT',
                                    ],
                                'created'                       => '2020-03-25T09:31:03.000+0000',
                                'reporter'                      =>
                                    [
                                        'self'         => 'http://jira.magic-estimation.dev.local/rest/api/2/user?username=mohammed.kharbotli',
                                        'name'         => 'mohammed.kharbotli',
                                        'key'          => 'JIRAUSER10000',
                                        'emailAddress' => 'mohammed.kharbotli@gmail.com',
                                        'avatarUrls'   =>
                                            [
                                                '48x48' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=48',
                                                '24x24' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=24',
                                                '16x16' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=16',
                                                '32x32' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=32',
                                            ],
                                        'displayName'  => 'mohammed kharbotli',
                                        'active'       => true,
                                        'timeZone'     => 'GMT',
                                    ],
                                'aggregateprogress'             =>
                                    [
                                        'progress' => 0,
                                        'total'    => 0,
                                    ],
                                'priority'                      =>
                                    [
                                        'self'    => 'http://jira.magic-estimation.dev.local/rest/api/2/priority/3',
                                        'iconUrl' => 'http://jira.magic-estimation.dev.local/images/icons/priorities/medium.svg',
                                        'name'    => 'Medium',
                                        'id'      => '3',
                                    ],
                                'customfield_10100'             => '0|i00007:',
                                'customfield_10101'             => NULL,
                                'environment'                   => NULL,
                                'timeestimate'                  => NULL,
                                'aggregatetimeoriginalestimate' => NULL,
                                'duedate'                       => NULL,
                                'progress'                      =>
                                    [
                                        'progress' => 0,
                                        'total'    => 0,
                                    ],
                                'votes'                         =>
                                    [
                                        'self'     => 'http://jira.magic-estimation.dev.local/rest/api/2/issue/PROJ-2/votes',
                                        'votes'    => 0,
                                        'hasVoted' => false,
                                    ],
                                'worklog'                       =>
                                    [
                                        'startAt'    => 0,
                                        'maxResults' => 20,
                                        'total'      => 0,
                                    ],
                                'assignee'                      => NULL,
                                'updated'                       => '2020-03-26T21:07:01.000+0000',
                                'status'                        =>
                                    [
                                        'self'           => 'http://jira.magic-estimation.dev.local/rest/api/2/status/10000',
                                        'description'    => '',
                                        'iconUrl'        => 'http://jira.magic-estimation.dev.local/',
                                        'name'           => 'To Do',
                                        'id'             => '10000',
                                        'statusCategory' =>
                                            [
                                                'self'      => 'http://jira.magic-estimation.dev.local/rest/api/2/statuscategory/2',
                                                'id'        => 2,
                                                'key'       => 'new',
                                                'colorName' => 'blue-gray',
                                                'name'      => 'To Do',
                                            ],
                                    ],
                            ],
                    ],
                    [
                        'expand' => 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
                        'id'     => '10002',
                        'self'   => 'http://jira.magic-estimation.dev.local/rest/agile/1.0/issue/10002',
                        'key'    => 'PROJ-3',
                        'fields' =>
                            [
                                'issuetype'                     =>
                                    [
                                        'self'        => 'http://jira.magic-estimation.dev.local/rest/api/2/issuetype/10001',
                                        'id'          => '10001',
                                        'description' => 'Created by Jira Software - do not edit or delete. Issue type for a user story.',
                                        'iconUrl'     => 'http://jira.magic-estimation.dev.local/images/icons/issuetypes/story.svg',
                                        'name'        => 'Story',
                                        'subtask'     => false,
                                    ],
                                'timespent'                     => NULL,
                                'timeoriginalestimate'          => NULL,
                                'sprint'                        =>
                                    [
                                        'id'            => 1,
                                        'self'          => 'http://jira.magic-estimation.dev.local/rest/agile/1.0/sprint/1',
                                        'state'         => 'future',
                                        'name'          => 'PROJ Sprint 1',
                                        'originBoardId' => 1,
                                        'goal'          => null,
                                    ],
                                'description'                   => NULL,
                                'project'                       =>
                                    [
                                        'self'           => 'http://jira.magic-estimation.dev.local/rest/api/2/project/10000',
                                        'id'             => '10000',
                                        'key'            => 'PROJ',
                                        'name'           => 'Project',
                                        'projectTypeKey' => 'software',
                                        'avatarUrls'     =>
                                            [
                                                '48x48' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?avatarId=10324',
                                                '24x24' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?size=small&avatarId=10324',
                                                '16x16' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?size=xsmall&avatarId=10324',
                                                '32x32' => 'http://jira.magic-estimation.dev.local/secure/projectavatar?size=medium&avatarId=10324',
                                            ],
                                    ],
                                'aggregatetimespent'            => NULL,
                                'resolution'                    => NULL,
                                'customfield_10106'             => 0.5,
                                'attachment'                    => [],
                                'aggregatetimeestimate'         => NULL,
                                'flagged'                       => false,
                                'resolutiondate'                => NULL,
                                'workratio'                     => -1,
                                'summary'                       => 'some new ticket',
                                'lastViewed'                    => '2020-03-26T09:26:52.393+0000',
                                'watches'                       =>
                                    [
                                        'self'       => 'http://jira.magic-estimation.dev.local/rest/api/2/issue/PROJ-3/watchers',
                                        'watchCount' => 1,
                                        'isWatching' => true,
                                    ],
                                'creator'                       =>
                                    [
                                        'self'         => 'http://jira.magic-estimation.dev.local/rest/api/2/user?username=mohammed.kharbotli',
                                        'name'         => 'mohammed.kharbotli',
                                        'key'          => 'JIRAUSER10000',
                                        'emailAddress' => 'mohammed.kharbotli@gmail.com',
                                        'avatarUrls'   =>
                                            [
                                                '48x48' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=48',
                                                '24x24' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=24',
                                                '16x16' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=16',
                                                '32x32' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=32',
                                            ],
                                        'displayName'  => 'mohammed kharbotli',
                                        'active'       => true,
                                        'timeZone'     => 'GMT',
                                    ],
                                'created'                       => '2020-03-25T09:31:16.000+0000',
                                'reporter'                      =>
                                    [
                                        'self'         => 'http://jira.magic-estimation.dev.local/rest/api/2/user?username=mohammed.kharbotli',
                                        'name'         => 'mohammed.kharbotli',
                                        'key'          => 'JIRAUSER10000',
                                        'emailAddress' => 'mohammed.kharbotli@gmail.com',
                                        'avatarUrls'   =>
                                            [
                                                '48x48' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=48',
                                                '24x24' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=24',
                                                '16x16' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=16',
                                                '32x32' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=32',
                                            ],
                                        'displayName'  => 'mohammed kharbotli',
                                        'active'       => true,
                                        'timeZone'     => 'GMT',
                                    ],
                                'aggregateprogress'             =>
                                    [
                                        'progress' => 0,
                                        'total'    => 0,
                                    ],
                                'priority'                      =>
                                    [
                                        'self'    => 'http://jira.magic-estimation.dev.local/rest/api/2/priority/3',
                                        'iconUrl' => 'http://jira.magic-estimation.dev.local/images/icons/priorities/medium.svg',
                                        'name'    => 'Medium',
                                        'id'      => '3',
                                    ],
                                'customfield_10100'             => '0|i0000f:',
                                'customfield_10101'             => NULL,
                                'environment'                   => NULL,
                                'timeestimate'                  => NULL,
                                'aggregatetimeoriginalestimate' => NULL,
                                'duedate'                       => NULL,
                                'progress'                      =>
                                    [
                                        'progress' => 0,
                                        'total'    => 0,
                                    ],
                                'votes'                         =>
                                    [
                                        'self'     => 'http://jira.magic-estimation.dev.local/rest/api/2/issue/PROJ-3/votes',
                                        'votes'    => 0,
                                        'hasVoted' => false,
                                    ],
                                'worklog'                       =>
                                    [
                                        'startAt'    => 0,
                                        'maxResults' => 20,
                                        'total'      => 0,
                                    ],
                                'assignee'                      =>
                                    [
                                        'self'         => 'http://jira.magic-estimation.dev.local/rest/api/2/user?username=mohammed.kharbotli',
                                        'name'         => 'mohammed.kharbotli',
                                        'key'          => 'JIRAUSER10000',
                                        'emailAddress' => 'mohammed.kharbotli@gmail.com',
                                        'avatarUrls'   =>
                                            [
                                                '48x48' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=48',
                                                '24x24' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=24',
                                                '16x16' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=16',
                                                '32x32' => 'https://www.gravatar.com/avatar/92e1acd5c0466abb87dd4e388683a268?d=mm&s=32',
                                            ],
                                        'displayName'  => 'mohammed kharbotli',
                                        'active'       => true,
                                        'timeZone'     => 'GMT',
                                    ],
                                'updated'                       => '2020-03-26T11:36:34.000+0000',
                                'status'                        =>
                                    [
                                        'self'           => 'http://jira.magic-estimation.dev.local/rest/api/2/status/10000',
                                        'description'    => '',
                                        'iconUrl'        => 'http://jira.magic-estimation.dev.local/',
                                        'name'           => 'To Do',
                                        'id'             => '10000',
                                        'statusCategory' =>
                                            [
                                                'self'      => 'http://jira.magic-estimation.dev.local/rest/api/2/statuscategory/2',
                                                'id'        => 2,
                                                'key'       => 'new',
                                                'colorName' => 'blue-gray',
                                                'name'      => 'To Do',
                                            ],
                                    ],
                            ],
                    ],
                ],
        ];
        $mapper = new IssueMapper($data);
        $issues = $mapper->map();
        self::assertIsArray($issues);
        self::assertContainsOnlyInstancesOf(Issue::class, $issues);
        self::assertCount(3, $issues);
    }

    public function constructor_data_provider(): Generator
    {
        yield [3.3, TypeError::class];
        yield [111, TypeError::class,];
        yield ['string', TypeError::class,];
        yield [(object) 'test', TypeError::class,];
        yield [['test' => 'test'], InvalidArgumentException::class];
        yield [['issues' => 'test'], InvalidArgumentException::class];
    }

    public function map_data_provider(): Generator
    {
        yield [['issues' => ['id' => 'string']], TypeError::class];
        yield [['issues' => [['id' => 'string', 'key' => 'string']]], InvalidArgumentException::class];
        yield [['issues' => [['id' => 'string', 'key' => 'string', 'fields' => 'string']]], InvalidArgumentException::class];
        yield [['issues' => [['id' => 'string', 'key' => 'string', 'fields' => []]]], InvalidArgumentException::class];
        yield [
            ['issues' => [['id' => 'string', 'key' => 'string', 'fields' => ['issuetype' => 'string']]]], InvalidArgumentException::class,
        ];
        yield [
            [
                'issues' => [
                    [
                        'id'     => 'string',
                        'key'    => 'string',
                        'fields' => [
                            'issuetype' => [
                                'id'        => 'id',
                                'iconUrl'   => '',
                                'name'      => 'name',
                                'workratio' => 'workratio',
                                'created'   => 'created',
                                'assignee'  => 'assignee',
                                'status'    => 'status',
                            ],
                        ],
                    ],
                ],
            ],
            InvalidArgumentException::class,
        ];
        yield [
            [
                'issues' => [
                    [
                        'id'     => 'string',
                        'key'    => 'string',
                        'fields' => [
                            'issuetype'   => [
                                'id'        => 'id',
                                'iconUrl'   => '',
                                'name'      => 'name',
                                'workratio' => 'workratio',
                                'created'   => 'created',
                                'assignee'  => 'assignee',
                                'status'    => [
                                    'name',
                                ],
                            ],
                            'description' => 'description',
                            'summary'     => 'summary',
                            'sprint'      => 'sprint',
                        ],
                    ],
                ],
            ],
            InvalidArgumentException::class,
        ];
        yield [
            [
                'issues' => [
                    [
                        'id'     => 'string',
                        'key'    => 'string',
                        'fields' => [
                            'issuetype'   => [
                                'id'      => 'id',
                                'iconUrl' => '',
                                'name'    => 'name',
                            ],
                            'workratio'   => 'workratio',
                            'created'     => 'created',
                            'updated'     => 'created',
                            'assignee'    => 'assignee',
                            'status'      => [
                                'name' => 'name',
                            ],
                            'description' => 'description',
                            'summary'     => 'summary',
                            'sprint'      => ['id' => 'id'],
                        ],
                    ],
                ],
            ],
            InvalidArgumentException::class,
        ];
    }

}
