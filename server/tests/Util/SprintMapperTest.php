<?php
declare(strict_types = 1);

namespace Tests\Util;

use App\Model\Sprint;
use App\Util\SprintMapper;
use Generator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use TypeError;

class SprintMapperTest extends TestCase
{

    /**
     * * @dataProvider constructor_data_provider
     * @param mixed  $data
     * @param string $expectedError
     */
    public function testPassWrongValueWoConstructor($data, string $expectedError)
    {
        self::expectException($expectedError);
        new SprintMapper($data);
    }

    /**
     * * @dataProvider map_data_provider
     * @param mixed  $data
     * @param string $expectedError
     */
    public function testMapWrongData(array $data, string $expectedError)
    {
        self::expectException($expectedError);
        $mapper = new SprintMapper($data);

        $mapper->map();
    }

    public function testWithCorrectData()
    {
        $data   =
            [
                "maxResults" => 50,
                "startAt"    => 0,
                "isLast"     => true,
                "values"     => [
                    [
                        "id"            => 1,
                        "self"          => "https://jira.magic-estimation.dev.local/rest/agile/1.0/sprint/1",
                        "state"         => "active",
                        "name"          => "AZH Sprint 5",
                        "startDate"     => "2020-03-10T14=>29=>03.772+01=>00",
                        "endDate"       => "2020-03-24T14=>29=>00.000+01=>00",
                        "originBoardId" => 181,
                        "goal"          => "",
                    ],
                    [
                        "id"            => '2',
                        "self"          => "https://jira.magic-estimation.dev.local/rest/agile/1.0/sprint/12",
                        "state"         => "active",
                        "name"          => "AZH Sprint 6",
                        "startDate"     => "2020-03-10T14=>29=>03.772+01=>00",
                        "endDate"       => "2020-03-24T14=>29=>00.000+01=>00",
                        "originBoardId" => 181,
                        "goal"          => "",
                    ],
                ],
            ];
        $mapper = new SprintMapper($data);
        $boards = $mapper->map();
        self::assertIsArray($boards);
        self::assertCount(2, $boards);
        self::assertContainsOnlyInstancesOf(Sprint::class, $boards);
    }

    public function constructor_data_provider(): Generator
    {
        yield [111, TypeError::class,];
        yield [3.3, TypeError::class];
        yield ['string', TypeError::class,];
        yield [(object) 'test', TypeError::class,];
    }

    public function map_data_provider(): Generator
    {
        yield [['id' => ''], InvalidArgumentException::class];
        yield [[], InvalidArgumentException::class];
        yield [['values' => ['id' => 'string']], TypeError::class];
        yield [['values' => [['name' => 'string']]], InvalidArgumentException::class];
        yield [['values' => [['id' => 'string']]], InvalidArgumentException::class];
        yield [['values' => [['id' => 'string', 'name' => 'string']]], InvalidArgumentException::class];
        yield [['values' => [['id' => 'string', 'name' => 'string', 'state' => 'string']]], InvalidArgumentException::class];
        yield [
            ['values' => [['id' => 'string', 'name' => 'string', 'state' => 'string', 'originBoardId' => 'id']]],
            InvalidArgumentException::class,
        ];
        yield [
            ['values' => [['id' => 'string', 'name' => 'string', 'state' => 'string', 'originBoardId' => '1']]],
            InvalidArgumentException::class,
        ];
    }

}
