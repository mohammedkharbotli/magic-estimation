export const state = () => ({
  sprints: [],
  issues: [],
  projects: [],
  boardId: '',
  sprint: '',
  goal: '',
  groups: [
    { model: [], label: '0.5', value: '0.5', type: 'primary' },
    { model: [], label: '2', value: '2', type: 'success' },
    { model: [], label: '5', value: '5', type: 'warning' },
    { model: [], label: '13', value: '13', type: 'danger' }
  ]
})

export const mutations = {
  setSprintGoal(state, goal) {
    state.goal = goal
  },
  setSprints(state, sprints) {
    state.sprints = sprints
  },
  setSprint(state, sprint) {
    state.sprint = sprint
  },
  setBoardId(state, boardId) {
    state.boardId = boardId
  },
  setIssues(state, issues) {
    state.groups.forEach((group) => {
      group.model.forEach((element) => {
        issues = issues.filter((item) => {
          return item.id !== element.id
        })
      })
    })
    state.issues = issues
  },
  setProjects(state, projects) {
    state.projects = projects
  },
  eraseGroups(state) {
    state.groups = [
      { model: [], label: '0.5', value: '0.5', type: 'primary' },
      { model: [], label: '2', value: '2', type: 'success' },
      { model: [], label: '5', value: '5', type: 'warning' },
      { model: [], label: '13', value: '13', type: 'danger' }
    ]
  },
  updateElement(state, data) {
    const item = data.element
    item.workRatio = data.point
    const groups = state.groups
    groups.forEach((group, index) => {
      state.groups[index].model = group.model.filter((e) => {
        return e.id !== item.id
      })
    })
    if (data.point) {
      groups.forEach((group, index) => {
        if (group.value === item.workRatio) {
          state.groups[index].model.push(item)
        }
      })
      state.issues = state.issues.filter((e) => {
        return e.id !== item.id
      })
    } else {
      state.issues.push(item)
    }
  }
}

export const actions = {
  async loadIssues({ state, commit }) {
    commit('setIssues', [])
    const res = await this.$axios.get('getAllIssues/' + state.sprint)
    commit('setIssues', res.data)
  },
  async loadProjects({ commit }) {
    const res = await this.$axios.get('/getProjects')
    commit('setProjects', res.data)
  },
  eraseStorage({ commit }) {
    commit('eraseGroups', [])
    commit('setProjects', [])
    commit('setSprint', '')
    commit('setSprints', [])
    commit('setIssues', [])
    commit('setSprintGoal', '')
    commit('setBoardId', '')
  }
}
export const getters = {
  sprintGoal(state) {
    return state.goal
  },
  sprint(state) {
    return state.sprint
  },
  sprints(state) {
    return state.sprints
  },
  groups(state) {
    return state.groups
  },
  issues(state) {
    return state.issues
  },
  projects(state) {
    return state.projects
  },
  boardId(state) {
    return state.boardId
  }
}
