const express = require('express')
const bodyParser = require('body-parser')
const axios = require('axios')
const app = express()
app.use(bodyParser())

app.get('/getAllIssues/:sprint', async function(req, res) {
  await axios
    .get('http://magic-estimation.dev.local/api/issues/' + req.params.sprint, {
      headers: {
        Accept: 'application/json'
      }
    })
    .then((response) => {
      res.send(response.data)
    })
})

app.get('/getProjects', async function(req, res) {
  await axios
    .get('http://magic-estimation.dev.local/api/boards/', {
      headers: {
        Accept: 'application/json'
      }
    })
    .then((response) => {
      res.send(response.data)
    })
})

app.get('/getSprints/:sprint', async function(req, res) {
  await axios
    .get(
      'http://magic-estimation.dev.local/api/board/' +
        req.params.sprint +
        '/sprint',
      {
        headers: {
          Accept: 'application/json'
        }
      }
    )
    .then((response) => {
      res.send(response.data)
    })
})

app.get('/checkSprint/:boardId/:sprintId', async function(req, res) {
  await axios
    .get(
      'http://magic-estimation.dev.local/api/sprint/check/' +
        req.params.boardId +
        '/' +
        req.params.sprintId,
      {
        headers: {
          Accept: 'application/json'
        }
      }
    )
    .then((response) => {
      res.send(response.data)
    })
})

app.post('/updateIssues/:sprint', async function(req, res) {
  await axios({
    method: 'post',
    url:
      'http://magic-estimation.dev.local/api/issue/' +
      req.params.sprint +
      '/update/',
    data: req.body
  })
    .then((response) => {
      res.send(response.data)
    })
    .catch((response) => {
      res.send({
        data: response.response.data,
        status: response.response.status
      })
    })
})

module.exports = {
  path: '/api/',
  handler: app
}
