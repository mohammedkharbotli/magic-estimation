# Magic Estimation

> Magic-Estimation ist eine schnelle Schätztechnik und besonders wertvoll für die zügige Schätzung einer großen Menge von User-Stories
> „Tickets“.
> Die Vorteile des magischen Schätzens sind die Schnelligkeit und die Subjektivität, mit der jedes Teammitglied auf den Prozess blicken
> darf. 


## Build Frontend

``` bash
# map to client folder:
$ cd client/

# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Build Backend

also yon need to run the Service in Backend.
 first you need to update the `.env` File:
 
``` bash
# go back to the Server folder:
$ cd ../server/

# generate .env file
$ cp .env.dist .env
```

in [Jira Api Documentation](https://developer.atlassian.com/server/jira/platform/oauth/) 
you can find the proper way to get the permissions and authorization data that needed for `.env` variables. 

``` bash
# run and build docker-machine
$ dc up -d --build

# install dependencies
$ dc exec app composer install

```

after successfully building the application the tool will be available on: [localhost:3000](localhost:3000) 

## Configure Jira Locally
in order to be able to use the tool locally you need to install and configure Jira.

 - open the link  [jira.magic-estimation.dev.local](http://jira.magic-estimation.dev.local).
 - follow the instructions on [Jira's Documentation](https://confluence.atlassian.com/adminjiraserver/running-the-setup-wizard-938846872.html).

## Jira Docker Image
you can download the Jira-Docker image from [Docker hub](https://hub.docker.com/r/atlassian/jira-software).
and then follow the instructions on the same page.

## Enable Custom-Fields in Jira
if the Story-Points field wasn't activated to your Project, you need to activated it.
to do that you can follow the instructions [here](https://support.atlassian.com/jira-software-cloud/docs/configure-estimation-and-tracking/).

## Swagger
under the link [swagger.magic-estimation.dev.local](http://swagger.magic-estimation.dev.local) you can find the documentation to the api
 routes.
